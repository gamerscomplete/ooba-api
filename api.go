// websocket_streamer.go
package websocket_streamer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var DefaultPayload = RequestPayload{
	Prompt:            "",
	MaxNewTokens:      1000,
	DoSample:          true,
	Temperature:       1.3,
	TopP:              0.1,
	TypicalP:          1,
	RepetitionPenalty: 1.18,
	TopK:              40,
	MinLength:         0,
	NoRepeatNgramSize: 0,
	NumBeams:          1,
	PenaltyAlpha:      0,
	LengthPenalty:     1,
	EarlyStopping:     false,
	Seed:              -1,
	AddBosToken:       true,
	TruncationLength:  2048,
	BanEosToken:       false,
	SkipSpecialTokens: true,
	StoppingStrings:   []string{},
}

type RequestPayload struct {
	Prompt            string   `json:"prompt"`
	MaxNewTokens      int      `json:"max_new_tokens"`
	DoSample          bool     `json:"do_sample"`
	Temperature       float64  `json:"temperature"`
	TopP              float64  `json:"top_p"`
	TypicalP          float64  `json:"typical_p"`
	RepetitionPenalty float64  `json:"repetition_penalty"`
	TopK              int      `json:"top_k"`
	MinLength         int      `json:"min_length"`
	NoRepeatNgramSize int      `json:"no_repeat_ngram_size"`
	NumBeams          int      `json:"num_beams"`
	PenaltyAlpha      int      `json:"penalty_alpha"`
	LengthPenalty     int      `json:"length_penalty"`
	EarlyStopping     bool     `json:"early_stopping"`
	Seed              int      `json:"seed"`
	AddBosToken       bool     `json:"add_bos_token"`
	TruncationLength  int      `json:"truncation_length"`
	BanEosToken       bool     `json:"ban_eos_token"`
	SkipSpecialTokens bool     `json:"skip_special_tokens"`
	StoppingStrings   []string `json:"stopping_strings"`
}

type streamReponse struct {
	Event string `json:"event"`
	Text  string `json:"text"`
}

type Response struct {
	Results []struct {
		Text string `json:"text"`
	} `json:"results"`
}

func RunStream(prompt string, uri string, payload RequestPayload, writer io.Writer) {
	payload.Prompt = prompt

	c, _, err := websocket.DefaultDialer.Dial(uri, nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	requestBytes, _ := json.Marshal(payload)
	err = c.WriteMessage(websocket.TextMessage, requestBytes)
	if err != nil {
		log.Fatal("write:", err)
	}

	fmt.Fprint(writer, prompt)

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Fatal("read:", err)
		}

		var response streamReponse
		err = json.Unmarshal(message, &response)
		if err != nil {
			log.Fatal("json unmarshal:", err)
		}

		switch response.Event {
		case "text_stream":
			fmt.Fprint(writer, response.Text)
		case "stream_end":
			return
		}
	}
}

func Run(prompt string, uri string, payload RequestPayload) (string, error) {
	payload.Prompt = prompt

	requestJSON, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}

	resp, err := http.Post(uri, "application/json", bytes.NewBuffer(requestJSON))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", errors.New("Error: non-200 status code returned: " + strconv.Itoa(resp.StatusCode))
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}
	result := response.Results[0].Text

	return result, nil
}
