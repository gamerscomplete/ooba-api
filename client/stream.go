// main.go
package main

import (
	"gitlab.com/gamerscomplete/ooba-api"
	"os"
)

const (
	// For local streaming, the websockets are hosted without SSL - ws://
	host = "localhost:5005"
	uri  = "ws://" + host + "/api/v1/stream"

	// For reverse-proxied streaming, the remote will likely host with SSL - wss://
	// uri = "wss://your-uri-here.trycloudflare.com/api/v1/stream"
)

func main() {
	prompt := "here is a basic webserver application in golang"
	payload := websocket_streamer.DefaultPayload
	websocket_streamer.RunStream(prompt, uri, payload, os.Stdout)
}
