// main.go
package main

import (
	"fmt"
	"gitlab.com/gamerscomplete/ooba-api"
)

const (
	// For local streaming, the websockets are hosted without SSL - ws://
    host = "localhost:5000"
	uri  = "http://" + host + "/api/v1/generate"

	// For reverse-proxied streaming, the remote will likely host with SSL - wss://
	// uri = "wss://your-uri-here.trycloudflare.com/api/v1/stream"
)

func main() {
	prompt := "here is a basic webserver application in golang"
	payload := websocket_streamer.DefaultPayload
	response, err := websocket_streamer.Run(prompt, uri, payload)
	if err != nil {
		fmt.Println("Failed:", err)
		return
	}
	fmt.Println("Response:", response)
}
